# Getting Started (Gradle)


## Scaffold

    spring init --name=WebFlux2 -g=spring5 -a=webflux2 -d=webflux,devtools,actuator --boot=2.0.0.BUILD-SNAPSHOT --build=gradle  spring5-webflux2
    spring init --name=WebFlux2Client -g=spring5 -a=webflux2.client -d=spring-shell,webflux,devtools --boot=2.0.0.BUILD-SNAPSHOT --build=gradle  spring5-webflux2-client



## Build

    ./gradlew clean build



## Run

    ./gradlew bootRun





