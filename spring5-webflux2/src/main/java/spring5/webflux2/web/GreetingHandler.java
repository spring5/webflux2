/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring5.webflux2.web;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.EntityResponse.fromObject;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import spring5.webflux2.domain.Greeting;

/**
 *
 * @author harry
 */
public class GreetingHandler {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    public Mono<ServerResponse> getGreeting(ServerRequest request) {
        String name;
        Optional<String> nameParam = request.queryParam("name");
        if (nameParam.isPresent()) {
            name = nameParam.get();
        } else {
            name = "World";   // Default value.values
        }
        
//        Mono<ServerResponse> notFound = ServerResponse.notFound().build();
//        Mono<Greeting> greetingMono = Mono.just(new Greeting(counter.incrementAndGet(),
//                String.format(template, name)));
//        return greetingMono
//                .then(greeting -> ServerResponse.ok().contentType(APPLICATION_JSON).body(fromObject(greeting)))
//                .otherwiseIfEmpty(notFound);

        Mono<Greeting> greetingMono = Mono.just(new Greeting(counter.incrementAndGet(),
                String.format(template, name)));
        return ServerResponse.ok().contentType(APPLICATION_JSON)
                .body(greetingMono, Greeting.class);
    }

}
