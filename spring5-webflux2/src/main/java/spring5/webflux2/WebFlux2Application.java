package spring5.webflux2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import spring5.webflux2.web.GreetingHandler;

@SpringBootApplication
@EnableWebFlux
public class WebFlux2Application {

    @Bean
    GreetingHandler greetingHandler() {
        return new GreetingHandler();
    }

    @Bean
    RouterFunction<ServerResponse> greetingRouterFunction(GreetingHandler greetingHandler) {
        RouterFunction<ServerResponse> routerFunction
                = RouterFunctions.route(RequestPredicates.path("/greeting"), greetingHandler::getGreeting);
        return routerFunction;
    }

    public static void main(String[] args) {
        SpringApplication.run(WebFlux2Application.class, args);
    }
}
